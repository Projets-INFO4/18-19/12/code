/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.dlinkupnpcamera.internal;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.smarthome.core.thing.ThingTypeUID;

import com.google.common.collect.Sets;

/**
 * The {@link DlinkUpnpCameraBinding} class defines common constants, which are
 * used across the whole binding.
 *
 * @author Yacine Ndiaye
 * @author Antoine Blanc
 * @author Christopher Law
 */
public class DlinkUpnpCameraBindingConstants {

    public static final String BINDING_ID = "dlinkupnpcamera";

    // List of all Thing Type UIDs
    public final static ThingTypeUID CAMERA_THING_TYPE_UID = new ThingTypeUID(BINDING_ID, "camera");

    public static final Set<ThingTypeUID> SUPPORTED_KNOWN_THING_TYPES_UIDS = Sets.newHashSet(CAMERA_THING_TYPE_UID);

    public static final Set<ThingTypeUID> SUPPORTED_THING_TYPES_UIDS = new HashSet<ThingTypeUID>(
            SUPPORTED_KNOWN_THING_TYPES_UIDS);

    // List of all Channel ids
    public final static String PAN = "pan";
    public final static String TILT = "tilt";
    public final static String IMAGE = "image";

}
