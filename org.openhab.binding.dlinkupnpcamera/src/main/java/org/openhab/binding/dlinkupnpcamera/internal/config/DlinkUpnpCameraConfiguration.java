/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.dlinkupnpcamera.internal.config;

/**
 * The {@link DlinkUpnpCameraConfiguration} contains parameters for the configuration of a camera.
 *
 * @author Yacine Ndiaye
 * @author Antoine Blanc
 * @author Christopher Law
 */
public class DlinkUpnpCameraConfiguration {

    public static final String UDN = "udn";
    public static final String IP = "ip";
    public static final String NAME = "name";

    public String udn;
    public String username;
    public String password;
    public String commandRequest;
    public String imageRequest;
    public Integer connectionRefresh;

}
